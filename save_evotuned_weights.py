
# coding: utf-8

# # Save the large evotuned weights for normal access
# 
# The evotuned weights are saved as a TF checkpoint file, unlike all the other weight files which are stored and loaded as NP files. This loads and saves the evotuned weights so it matches the others.

# In[2]:


get_ipython().system('aws s3 sync --no-sign-request --quiet s3://unirep-public/evotuned/unirep/ evotuned/unirep/')


# In[3]:


get_ipython().system('ls -lah evotuned/unirep/')


# In[4]:


from tensorflow.python.tools.inspect_checkpoint import print_tensors_in_checkpoint_file
# ?print_tensors_in_checkpoint_file
# print_tensors_in_checkpoint_file(file_name='./tmp/mdl_ckpt', tensor_name='', all_tensors=False)
print_tensors_in_checkpoint_file('evotuned/unirep/model-13560', tensor_name='', all_tensors=True)


# In[ ]:


from unirep import babbler1900 as babbler

TEMPLATE_MODEL_WEIGHT_PATH = "./1900_weights"
BATCH_SIZE = 12

b = babbler(batch_size=BATCH_SIZE, model_path=TEMPLATE_MODEL_WEIGHT_PATH, )


# In[ ]:


get_ipython().system('mkdir 1900_weights_evotuned')


# In[ ]:


import tensorflow as tf

saver = tf.train.Saver()
with tf.Session() as sess:
    saver.restore(sess, 'evotuned/unirep/model-13560')
    b.dump_weights(sess, dir_name='./1900_weights_evotuned')

