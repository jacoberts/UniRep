
# coding: utf-8

# In[1]:


get_ipython().system('ls')


# In[1]:


MODEL_WEIGHT_PATH =  "./1900_weights"  # "./1900_weights_evotuned"
EMBEDDINGS_MEAN_FNAME = 'unirep_fp_base_embeddings2_mean.csv'
EMBEDDINGS_FINAL_FNAME = 'unirep_fp_base_embeddings2_final.csv'


# In[4]:


get_ipython().system('pip install biopython')


# In[2]:


get_ipython().run_cell_magic('time', '', '# import pandas\n# exp1_variants = pandas.read_csv(\'exp1_variants.csv\')\n# aa_sequences = exp1_variants.aaSequence.tolist()\n# truth_labels = exp1_variants.medianBrightness > 2.2\n\nfrom Bio import SeqIO\n\naa_sequences = []\ntruth_labels = []\nfor seq_record in SeqIO.parse("fp_base_homologs2.fasta", "fasta"):\n    aa_sequences.append(str(seq_record.seq))\n    truth_labels.append(seq_record.id.startswith(\'original\'))')


# In[ ]:


get_ipython().run_cell_magic('time', '', 'from unirep import babbler1900 as babbler\nimport tensorflow as tf\n\nBATCH_SIZE = 12\n\nb = babbler(batch_size=BATCH_SIZE, model_path=MODEL_WEIGHT_PATH, )\n\n\ndef initialize_uninitialized(sess):\n    """\n    from https://stackoverflow.com/questions/35164529/in-tensorflow-is-there-any-way-to-just-initialize-uninitialised-variables\n    """\n    global_vars = tf.global_variables()\n    is_not_initialized = sess.run([tf.is_variable_initialized(var) for var in global_vars])\n    not_initialized_vars = [v for (v, f) in zip(global_vars, is_not_initialized) if not f]\n    if len(not_initialized_vars):\n        sess.run(tf.variables_initializer(not_initialized_vars))')


# In[7]:


get_ipython().system('pip install tqdm')
get_ipython().system('pip install ipywidgets')
# !jupyter nbextension enable --py widgetsnbextension


# In[ ]:


get_ipython().run_cell_magic('time', '', 'from tqdm import tqdm as tqdm\n\nembeddings_mean = []\nembeddings_final = []\n\nwith tf.Session() as sess:\n    initialize_uninitialized(sess)\n    tf.get_default_graph().finalize()\n    \n    for seq in tqdm(aa_sequences):\n        avg_hidden, final_hidden, _ = b.get_rep(sess, seq)\n        embeddings_mean.append(avg_hidden)\n        embeddings_final.append(final_hidden)')


# In[ ]:


get_ipython().run_cell_magic('time', '', 'import numpy\nembeddings_mean_np = numpy.matrix(embeddings_mean)\nnumpy.savetxt(EMBEDDINGS_MEAN_FNAME, embeddings_mean_np, delimiter=",")\nembeddings_final_np = numpy.matrix(embeddings_final)\nnumpy.savetxt(EMBEDDINGS_FINAL_FNAME, embeddings_final_np, delimiter=",")')


# In[ ]:


get_ipython().run_cell_magic('time', '', 'from sklearn.manifold import TSNE\n\ntsne = TSNE(n_components=2, verbose=1, perplexity=40, n_iter=300)\ntsne_results = tsne.fit_transform(embeddings_mean_np)')


# In[ ]:


from matplotlib import pyplot
pyplot.scatter(
    tsne_results[[not t for t in truth_labels], 0],
    tsne_results[[not t for t in truth_labels], 1],
    alpha=0.05,
    c='red'#['green' if truth_label else 'red' for truth_label in truth_labels]
)
pyplot.scatter(
    tsne_results[truth_labels, 0],
    tsne_results[truth_labels, 1],
    alpha=0.05,
    c='green' #['green' if truth_label else 'red' for truth_label in truth_labels]
)

# pyplot.scatter
pyplot.show()

